package br.com.mastertech.cliente.dto.builder;

import br.com.mastertech.cliente.dto.ClienteRequest;

public final class ClienteRequestBuilder {
    private String nome;
    private String cep;

    private ClienteRequestBuilder() {
    }

    public static ClienteRequestBuilder aClienteRequest() {
        return new ClienteRequestBuilder();
    }

    public ClienteRequestBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public ClienteRequestBuilder cep(String cep) {
        this.cep = cep;
        return this;
    }

    public ClienteRequest build() {
        return new ClienteRequest(nome, cep);
    }
}
