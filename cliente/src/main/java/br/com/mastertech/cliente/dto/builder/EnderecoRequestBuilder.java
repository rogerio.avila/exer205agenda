package br.com.mastertech.cliente.dto.builder;

import br.com.mastertech.cliente.dto.EnderecoRequest;

public final class EnderecoRequestBuilder {
    private String nome;
    private String rua;
    private String bairro;
    private String cidade;
    private String estado;

    private EnderecoRequestBuilder() {
    }

    public static EnderecoRequestBuilder anEnderecoRequest() {
        return new EnderecoRequestBuilder();
    }

    public EnderecoRequestBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public EnderecoRequestBuilder rua(String rua) {
        this.rua = rua;
        return this;
    }

    public EnderecoRequestBuilder bairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public EnderecoRequestBuilder cidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public EnderecoRequestBuilder estado(String estado) {
        this.estado = estado;
        return this;
    }

    public EnderecoRequest build() {
        return new EnderecoRequest(nome, rua, bairro, cidade, estado);
    }
}
