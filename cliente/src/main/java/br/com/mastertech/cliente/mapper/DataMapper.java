package br.com.mastertech.cliente.mapper;

import br.com.mastertech.cliente.client.Cep;
import br.com.mastertech.cliente.dto.EnderecoRequest;
import br.com.mastertech.cliente.dto.ClienteRequest;
import br.com.mastertech.cliente.entity.EnderecoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataMapper {
    DataMapper INSTANCE = Mappers.getMapper(DataMapper.class);

    EnderecoRequest enderecoEntityToEnderecoRequest(EnderecoEntity enderecoEntity);
    List<EnderecoRequest> enderecoEntityToEnderecoRequest(List<EnderecoEntity> enderecoEntity);

    @Mappings({
            @Mapping(source = "cliente.nome", target = "nome"),
            @Mapping(source = "cep.logradouro", target = "rua"),
            @Mapping(source = "cep.bairro", target = "bairro"),
            @Mapping(source = "cep.localidade", target = "cidade"),
            @Mapping(source = "cep.uf", target = "estado")
    })
    EnderecoEntity clienteAndCepToEnderecoRegistryRequest(ClienteRequest cliente, Cep cep);
}
