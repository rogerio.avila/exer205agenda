package br.com.mastertech.cliente.entity;

import br.com.mastertech.cliente.entity.builder.EnderecoEntityBuilder;

import javax.persistence.*;

@Entity
@Table(name = "endereco")
public class EnderecoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String rua;
    private String bairro;
    private String cidade;
    private String estado;

    public EnderecoEntity() {
    }

    public EnderecoEntity(String nome, String rua, String bairro, String cidade, String estado) {
        this.nome = nome;
        this.rua = rua;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
    }

    public static EnderecoEntityBuilder builder() {
        return EnderecoEntityBuilder.anEnderecoEntity();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getEstado() {
        return estado;
    }
}
