package br.com.mastertech.cliente.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ZUUL", configuration = ResilienceConfiguration.class)
public interface ZuulClient {

    @GetMapping("/cep/{cep}")
    Cep getCep(@PathVariable("cep") String cep);
}
