package br.com.mastertech.cliente.client;

import br.com.mastertech.cliente.exception.CepServicoForadoArceDownException;

public class CepFallback implements ZuulClient {
    @Override
    public Cep getCep(String cep) {
        throw new CepServicoForadoArceDownException();
    }
}
