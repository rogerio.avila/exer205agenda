package br.com.mastertech.cliente.repository;

import br.com.mastertech.cliente.entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Long> {}
