package br.com.mastertech.cliente.dto;

import br.com.mastertech.cliente.dto.builder.ClienteRequestBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClienteRequest {
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("cep")
    private String cep;

    public ClienteRequest(String nome, String cep) {
        this.nome = nome;
        this.cep = cep;
    }

    public static ClienteRequestBuilder builder(){
        return ClienteRequestBuilder.aClienteRequest();
    }

    public String getNome() {
        return nome;
    }

    public String getCep() {
        return cep;
    }
}
