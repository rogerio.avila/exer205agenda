package br.com.mastertech.cliente.controller;

import br.com.mastertech.cliente.dto.EnderecoRequest;
import br.com.mastertech.cliente.dto.ClienteRequest;
import br.com.mastertech.cliente.entity.EnderecoEntity;
import br.com.mastertech.cliente.mapper.DataMapper;
import br.com.mastertech.cliente.service.EnderecoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@RestController
public class EnderecoController {

    private final EnderecoService enderecoService;

    public EnderecoController(EnderecoService enderecoService) {
        this.enderecoService = enderecoService;
    }

    @GetMapping("/telefones")
    public ResponseEntity registry() {
        List<EnderecoEntity> listOfEnderecos = enderecoService.getAllEnderecos();
        List<EnderecoRequest> enderecoRequests = DataMapper.INSTANCE.enderecoEntityToEnderecoRequest(listOfEnderecos);
        return ResponseEntity.ok().body(enderecoRequests);
    }

    @PostMapping("/telefones")
    public ResponseEntity post(@RequestBody ClienteRequest cliente) {
        EnderecoEntity enderecoEntity = enderecoService.enderecoCliente(cliente);
        EnderecoRequest enderecoRequest = DataMapper.INSTANCE.enderecoEntityToEnderecoRequest(enderecoEntity);
        return ResponseEntity.created(URI.create("")).body(enderecoRequest);
    }
}
