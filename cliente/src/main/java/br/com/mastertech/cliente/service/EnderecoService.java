package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.client.Cep;
import br.com.mastertech.cliente.client.ZuulClient;
import br.com.mastertech.cliente.dto.ClienteRequest;
import br.com.mastertech.cliente.entity.EnderecoEntity;
import br.com.mastertech.cliente.mapper.DataMapper;
import br.com.mastertech.cliente.repository.EnderecoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EnderecoService {

    private final EnderecoRepository enderecoRepository;
    private final ZuulClient zuulClient;

    public EnderecoService(EnderecoRepository enderecoRepository, ZuulClient zuulClient) {
        this.enderecoRepository = enderecoRepository;
        this.zuulClient = zuulClient;
    }

    public EnderecoEntity enderecoCliente(ClienteRequest cliente) {
        Cep cep = zuulClient.getCep(cliente.getCep());
        EnderecoEntity enderecoEntity = DataMapper.INSTANCE.clienteAndCepToEnderecoRegistryRequest(cliente, cep);
        return enderecoRepository.save(enderecoEntity);
    }

    public List<EnderecoEntity> getAllEnderecos() {
        Iterable<EnderecoEntity> iterable = enderecoRepository.findAll();
        return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
    }
}
