package br.com.mastertech.cliente.dto;

import br.com.mastertech.cliente.dto.builder.EnderecoRequestBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EnderecoRequest {
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("rua")
    private String rua;
    @JsonProperty("bairro")
    private String bairro;
    @JsonProperty("cidade")
    private String cidade;
    @JsonProperty("estado")
    private String estado;

    public EnderecoRequest(String nome, String rua, String bairro, String cidade, String estado) {
        this.nome = nome;
        this.rua = rua;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
    }

    public static EnderecoRequestBuilder builder(){
        return EnderecoRequestBuilder.anEnderecoRequest();
    }

    public String getNome() {
        return nome;
    }

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getEstado() {
        return estado;
    }
}
