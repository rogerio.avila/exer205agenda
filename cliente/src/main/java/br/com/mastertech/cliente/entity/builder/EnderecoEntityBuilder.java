package br.com.mastertech.cliente.entity.builder;

import br.com.mastertech.cliente.entity.EnderecoEntity;

public final class EnderecoEntityBuilder {
    private String nome;
    private String rua;
    private String bairro;
    private String cidade;
    private String estado;

    private EnderecoEntityBuilder() {
    }

    public static EnderecoEntityBuilder anEnderecoEntity() {
        return new EnderecoEntityBuilder();
    }

    public EnderecoEntityBuilder nome(String nome) {
        this.nome = nome;
        return this;
    }

    public EnderecoEntityBuilder rua(String rua) {
        this.rua = rua;
        return this;
    }

    public EnderecoEntityBuilder bairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public EnderecoEntityBuilder cidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public EnderecoEntityBuilder estado(String estado) {
        this.estado = estado;
        return this;
    }

    public EnderecoEntity build() {
        return new EnderecoEntity(nome, rua, bairro, cidade, estado);
    }
}
