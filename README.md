<h1 id="mastertech-sistema-de-cep">Consulta CEP</h1>

# Introdução
API para Cadastro de Cliente com busca de CEP na base dos Correios


<a id="opIdPostRegistry"></a>


```http
POST http://localhost:7000/cliente/telefones HTTP/1.1
Host: localhost:7000
Content-Type: application/json

```

## Cadastra novo Cliente com o nome e o CEP informados.

```json
{
  "nome": "Roger",
  "cep": "06030150"
}
```

## Retorna todos os contatos com telefones


```http
GET http://localhost:7000/cliente/telefones HTTP/1.1
Host: localhost:7000

```
