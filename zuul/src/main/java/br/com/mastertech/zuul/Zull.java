package br.com.mastertech.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class Zull {
	public static void main(String[] args) {
		SpringApplication.run(Zull.class, args);
	}
}
